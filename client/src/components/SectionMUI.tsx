import React from 'react';
import Box from '@mui/material/Box';

const SectionMUI = ({ children, ...props }: { children: React.ReactNode; [key: string]: any }) => {
  return (
    <Box
      component="section"
      sx={{
        p: 5,
        border: 'none'
      }}
      {...props}
    >
      {children}
    </Box>
  );
};

export default SectionMUI;
