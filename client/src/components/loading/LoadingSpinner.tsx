import React from 'react';
import Box from '@mui/material/Box';
import { CircularProgress } from '@mui/material';
import styles from './LoadingSpinner.module.scss';

const LoadingSpinner = () => {
  return (
    <Box
      className={`custom-flex-center position-absolute top-50 start-50 translate-middle ${styles.LoadingSpinner}`}
    >
      <CircularProgress />
    </Box>
  );
};

export default LoadingSpinner;
