import React from 'react';
import Container from '@mui/material/Container';

const CustomContainer = ({
  children,
  gap = 2,
  ...props
}: {
  children: React.ReactNode;
  gap?: number;
  [key: string]: any;
}) => {
  return (
    <Container className={`w-100 pe-0`} {...props}>
      {children}
    </Container>
  );
};

export default CustomContainer;
