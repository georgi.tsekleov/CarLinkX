import React from 'react';
import Grid from '@mui/material/Grid';

const CustomCol = ({ children, ...props }: { children: React.ReactNode; [key: string]: any }) => {
  return (
    <Grid item className={`p-0`} {...props}>
      {children}
    </Grid>
  );
};

export default CustomCol;
