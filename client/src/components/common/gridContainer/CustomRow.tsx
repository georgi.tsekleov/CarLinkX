import React from 'react';
import { Grid } from '@mui/material';

interface Props {
  children: React.ReactNode;
  gap?: number;

  [key: string]: any;
}

const CustomRow = ({ children, gap = 2, ...props }: Props) => {
  return (
    <Grid container spacing={2} {...props}>
      {children}
    </Grid>
  );
};

export default CustomRow;
