import { useEffect, useState } from 'react';
import { Post } from '@/app/types';

const useFetchPosts = () => {
  const [posts, setPosts] = useState<Post[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await fetch(`api/posts`, { method: 'GET' });
        if (response.ok) {
          const result = await response.json();
          setPosts(result);
        } else {
          setError(true);
        }
      } catch (err) {
        console.log('Error on fetch posts: ', err);
        setError(true);
      } finally {
        setLoading(false);
      }
    };

    fetchPosts();
  }, []);

  return { posts, loading, error };
};

export default useFetchPosts;
