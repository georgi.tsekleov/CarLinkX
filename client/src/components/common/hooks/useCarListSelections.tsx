import { Post } from '@/app/types';
import { SelectionState } from '@/app/list/types';
import { useEffect, useState } from 'react';

const initialSelectionsState: SelectionState = {
  price: [10, 100],
  region: 'Sofia'
};

const useCarListSelections = (carsList: Post[]) => {
  const [filteredCarsList, setFilteredCarsList] = useState<Post[]>([]);
  const [selections, setSelections] = useState(initialSelectionsState);

  const filterCarsList = (carsList: Post[], selections: SelectionState): Post[] => {
    const { price, region } = selections;
    const [minPrice, maxPrice] = price;

    return carsList.filter((post) => {
      const withinPriceRange = Number(post.price) >= minPrice && Number(post.price) <= maxPrice;
      const withinRegion = region ? post.city === region : true;
      return withinPriceRange && withinRegion;
    });
  };

  useEffect(() => {
    const filteredList = filterCarsList(carsList, selections);
    setFilteredCarsList(filteredList);
  }, [selections, carsList]);

  return { filteredCarsList, selections, setSelections };
};

export default useCarListSelections;
