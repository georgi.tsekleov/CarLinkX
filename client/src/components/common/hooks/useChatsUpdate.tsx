import { useEffect } from 'react';
import { useUserAuth } from '@/context/AuthContex';
import notificationStore from '@/store/notificationStore';
import useChatsStore from '@/store/chatsStore';

const useChatsUpdate = () => {
  const { currentUser } = useUserAuth();
  const { fetchNotification } = notificationStore();
  const { chats, fetchChats } = useChatsStore();

  useEffect(() => {
    if (currentUser) {
      fetchChats(currentUser.id);
    }
  }, [currentUser, fetchChats]);

  useEffect(() => {
    if (currentUser && chats) {
      fetchNotification(currentUser.id);
    }
  }, [currentUser, chats, fetchNotification]);
};

export default useChatsUpdate;
