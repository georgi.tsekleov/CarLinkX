import { useUserAuth } from '@/context/AuthContex';
import { MultipleChats } from '@/types';
import { updateSeenByChatsDB } from '@/utils/chats';
import chatsStore from '@/store/chatsStore';
import useSingleChatStore from '@/store/singleChatStore';

const useChatClick = () => {
  const { currentUser } = useUserAuth();
  const { setSelectedChat } = chatsStore();
  const { setOpenConversation } = useSingleChatStore();

  const handleChatClick = async (chat: MultipleChats) => {
    setSelectedChat(chat);
    setOpenConversation(true);

    if (currentUser) await updateSeenByChatsDB(chat.id, currentUser.id);
  };

  return { handleChatClick };
};

export default useChatClick;
