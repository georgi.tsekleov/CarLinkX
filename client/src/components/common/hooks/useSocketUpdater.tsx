import { useSocketContext } from '@/context/SocketContex';
import notificationStore from '@/store/notificationStore';
import useChatsStore from '@/store/chatsStore';
import { useEffect } from 'react';
import { Message } from '@/types';
import singleChatStore from '@/store/singleChatStore';
import chatsStore from '@/store/chatsStore';
import { useUserAuth } from '@/context/AuthContex';

const useSocketUpdater = () => {
  const socket = useSocketContext();
  const { currentUser } = useUserAuth();
  const { selectedChat } = chatsStore();
  const { updateChat, fetchSingleChat } = singleChatStore();
  const { increaseNumber } = notificationStore();
  const { updateChatsSeenBy, updateChatsLastMessage } = useChatsStore();

  useEffect(() => {
    if (selectedChat) {
      fetchSingleChat(selectedChat.id, currentUser?.id);
    }
  }, [selectedChat, fetchSingleChat, currentUser?.id]);

  useEffect(() => {
    if (socket) {
      socket.off('getMessage');
      socket.on('getMessage', (data: Message) => {
        updateChat(data);
        increaseNumber();
        updateChatsSeenBy(data);
        updateChatsLastMessage(data);
      });

      return () => {
        socket.off('getMessage');
      };
    }
  }, [socket, increaseNumber, updateChat, updateChatsLastMessage, updateChatsSeenBy]);
};

export default useSocketUpdater;
