import React, { useState } from 'react';
import AvatarSaveBtn from '@/components/common/avatar/AvatarSaveBtn';
import UploadWidget from '@/components/uploadWidget/UploadWidget';
import { CLOUD_NAME, MAX_SIZE_ONE_MB, UPLOAD_PRESET } from '../../../../state';
import { UploadedWidgetImg } from '@/types';

interface Props {
  avatar?: UploadedWidgetImg;
  showUploadBtn: boolean;
  onAvatarChange: (newAvatar: UploadedWidgetImg[]) => void;
}

const AvatarChangeAction = ({ avatar, showUploadBtn, onAvatarChange }: Props) => {
  const [avatarChanged, setAvatarChanged] = useState<boolean>(false);

  const handleAvatarChange = (newAvatar: UploadedWidgetImg[]) => {
    onAvatarChange(newAvatar);
    setAvatarChanged(true);
  };

  return avatarChanged ? (
    <AvatarSaveBtn avatar={avatar} onChangeAvatar={() => setAvatarChanged(false)} />
  ) : (
    <UploadWidget
      uwConfig={{
        cloudName: CLOUD_NAME,
        uploadPreset: UPLOAD_PRESET,
        maxImageFileSize: MAX_SIZE_ONE_MB,
        multiple: false,
        folder: 'avatars'
      }}
      setState={handleAvatarChange}
      showUploadBtn={showUploadBtn}
    />
  );
};

export default AvatarChangeAction;
