import React from 'react';
import { Button } from '@mui/material';
import { useUserAuth } from '@/context/AuthContex';
import { LOCAL_HOST_API } from '../../../../state';
import { UploadedWidgetImg } from '@/types';

interface Props {
  avatar: UploadedWidgetImg;
  onChangeAvatar: () => void;
}

const AvatarSaveBtn = ({ avatar, onChangeAvatar }: Props) => {
  const { currentUser } = useUserAuth();
  const { updateUser } = useUserAuth();

  const handleUpdateAvatar = async () => {
    try {
      const response = await fetch(`${LOCAL_HOST_API}/users/${currentUser?.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          avatar: [avatar]
        }),
        credentials: 'include'
      });

      if (response.ok) {
        const data = await response.json();

        updateUser(data);
        onChangeAvatar();
        console.log('Update avatar successfully');
      } else {
        console.log('Update error:', response.statusText);
      }
    } catch (error) {
      console.error('Register error:', error);
    }
  };

  return (
    <Button variant='contained' color='success' onClick={handleUpdateAvatar} className={`my-2`}>
      Save
    </Button>
  );
};

export default AvatarSaveBtn;
