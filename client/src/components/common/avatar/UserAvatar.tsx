import React, { useEffect, useState } from 'react';
import DefaultAvatarImg from '../../../../images/profile/def-avatar.png';
import Image from 'next/image';
import { UploadedWidgetImg, User } from '@/types';
import { Box } from '@mui/material';
import AvatarChangeAction from '@/components/common/avatar/AvatarChangeAction';

interface Props {
  children?: React.ReactNode;
  currentUser: User;
  width?: number;
  height?: number;
  showUploadBtn: boolean;
}

const UserAvatarUpload = ({ children, currentUser, width, height, showUploadBtn }: Props) => {
  const [avatar, setAvatar] = useState<UploadedWidgetImg>(currentUser?.avatar[0]);

  useEffect(() => {
    if (currentUser?.avatar) {
      setAvatar(currentUser.avatar[0]);
    }
  }, [currentUser]);

  const handleAvatarChange = (newAvatar: UploadedWidgetImg[]) => {
    setAvatar(newAvatar[0]);
  };

  return (
    <Box className={`position-relative custom-flex-center flex-column `}>
      <Image
        src={typeof avatar === 'string' ? avatar : DefaultAvatarImg}
        alt={`User avatar image`}
        width={width || 20}
        height={height || 20}
        className={`rounded-5 my-2`}
        priority
      />

      <AvatarChangeAction
        avatar={avatar}
        showUploadBtn={showUploadBtn}
        onAvatarChange={handleAvatarChange}
      />

      {avatar && children}
    </Box>
  );
};

export default UserAvatarUpload;
