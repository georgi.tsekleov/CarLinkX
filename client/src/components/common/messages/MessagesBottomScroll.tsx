import React, { useEffect, useRef } from 'react';
import { SingleChat } from '@/types';

const MessagesBottomScroll = ({ chat }: { chat: SingleChat | null }) => {
  const messagesEndRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(() => {
    scrollToBottom();
  }, [chat?.messages]);

  return <div ref={messagesEndRef} />;
};

export default MessagesBottomScroll;
