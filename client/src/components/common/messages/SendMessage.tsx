import { useUserAuth } from '@/context/AuthContex';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { Button } from '@mui/material';
import React from 'react';
import useChatsStore from '@/store/chatsStore';
import useChatClick from '@/components/common/hooks/useChatClick';

//TODO: Handle send first message!
const SendMessage = ({ receiverId }: { receiverId?: string }) => {
  const { currentUser } = useUserAuth();
  const { chats } = useChatsStore();
  const { handleChatClick } = useChatClick();

  const isMyPost = receiverId === currentUser?.id;
  const existChat = chats?.find((chat) => chat?.receiver?.id === receiverId);

  const addMessage = async () => {
    try {
      const response = await fetch('/api/chats/add-chat', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ receiverId, currentUserId: currentUser?.id })
      });

      await response.json();
    } catch (error) {
      console.error('Error fetching dynamic list page:', error);
    }
  };

  return (
    <Button
      variant="contained"
      color="primary"
      size="small"
      sx={{ my: 2 }}
      disabled={isMyPost}
      onClick={!!existChat ? () => handleChatClick(existChat) : addMessage}
    >
      <MailOutlineIcon />
    </Button>
  );
};

export default SendMessage;
