import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import SendIcon from '@mui/icons-material/Send';
import React, { useState } from 'react';
import { Message } from '@/types';
import chatsStore from '@/store/chatsStore';
import { useSocketContext } from '@/context/SocketContex';
import { updateSeenByChatsDB } from '@/utils/chats';
import singleChatStore from '@/store/singleChatStore';
import useChatsStore from '@/store/chatsStore';
import { useUserAuth } from '@/context/AuthContex';

const InputMessage = () => {
  const [message, setMessage] = useState('');
  const { currentUser } = useUserAuth();
  const socket = useSocketContext();
  const { updateChatsSeenBy, updateChatsLastMessage } = useChatsStore();
  const { selectedChat } = chatsStore();
  const { updateChat } = singleChatStore();

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(e.target.value);
  };

  const onUpdateChat = async (data: Message) => {
    updateChat(data);
    updateChatsSeenBy(data);

    if (currentUser) {
      await updateSeenByChatsDB(data.chatId, currentUser?.id);
    }
  };

  const handleSendClick = async () => {
    try {
      const response = await fetch(`/api/messages`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          text: message,
          selectedChatId: selectedChat?.id,
          currentUserId: currentUser?.id
        }),
        credentials: 'include'
      });

      if (!response.ok) {
        console.error('Could not send message: ', message);
        return;
      }

      const responseData = await response.json();

      if (socket && selectedChat?.receiver) {
        socket.emit('sendMessage', { receiverId: selectedChat?.receiver.id, data: responseData });
        await onUpdateChat(responseData);
      }

      setMessage('');
    } catch (e) {
      console.error('Error on send message: ', e);
    }
  };

  return (
    <TextField
      type="search"
      variant="standard"
      fullWidth
      label="Message"
      id="Message"
      value={message}
      onChange={handleInputChange}
      InputProps={{
        endAdornment: (
          <IconButton onClick={handleSendClick} disabled={message.length < 2}>
            <SendIcon fontSize="large" />
          </IconButton>
        )
      }}
    />
  );
};

export default InputMessage;
