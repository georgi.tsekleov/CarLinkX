import { Message } from '@/types';
import { Box } from '@mui/material';
import MessageOptions from '@/components/common/messages/MessageOptions';
import Typography from '@mui/material/Typography';
import styles from '@/components/common/messages/MessageItem.module.scss';
import React from 'react';
import { useUserAuth } from '@/context/AuthContex';
import { format } from 'timeago.js';

const MessageItem = ({ message }: { message: Message }) => {
  const { currentUser } = useUserAuth();

  const isMyMessage = (message: Message) => message.userId === currentUser?.id;

  return (
    <Box
      className={`d-flex align-items-center 
      ${isMyMessage(message) ? 'justify-content-end' : 'justify-content-start'}`}
    >
      <MessageOptions message={message} isMyMessage={isMyMessage(message)}>
        <Typography
          variant="caption"
          display="block"
          className={`position-relative m-1 p-2 pb-3 
          ${styles.Message} ${isMyMessage(message) ? `${styles.MyMessage} text-end` : `${styles.ReceiverMessage} text-start`}`}
        >
          {message.text}

          <Typography
            variant="caption"
            display="block"
            fontSize={8}
            className={`position-absolute bottom-2 px-1 bg-white text-dark rounded-3 
            ${isMyMessage(message) ? 'end-0 me-1' : 'start-0 ms-1'}`}
          >
            {format(message.createdAt)}
          </Typography>
        </Typography>
      </MessageOptions>
    </Box>
  );
};

export default MessageItem;
