import { Box } from '@mui/material';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import DeleteIcon from '@mui/icons-material/Delete';
import React, { useState } from 'react';
import { Message } from '@/types';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import styles from './MessageOptions.module.scss';

interface Props {
  children: React.ReactNode;
  message: Message;
  isMyMessage: boolean;
}

const MessageOptions = ({ children, message, isMyMessage }: Props) => {
  const [options, setOptions] = useState(false);
  const [clickedMessage, setClickedMessage] = useState<Message | null>(null);

  const showOptionsHandler = (message: Message) => {
    setOptions(!options);
    setClickedMessage(message);
  };

  const isClickedMessage = clickedMessage?.id === message.id;

  return (
    <React.Fragment>
      {isMyMessage && (
        <MoreHorizIcon
          className={`${styles.Options}`}
          onClick={() => showOptionsHandler(message)}
        />
      )}

      {isMyMessage && isClickedMessage && options && (
        <Box className={`d-flex justify-content-end`}>
          <BorderColorIcon className={`ms-2 ${styles.Options}`} fontSize="small" />
          <DeleteIcon className={`ms-2 ${styles.Options}`} fontSize="small" />
        </Box>
      )}

      {children}
    </React.Fragment>
  );
};

export default MessageOptions;
