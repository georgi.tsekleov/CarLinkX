import React, { useEffect } from 'react';
import Typography from '@mui/material/Typography';
import MessagesBottomScroll from '@/components/common/messages/MessagesBottomScroll';
import MessageItem from '@/components/common/messages/MessageItem';
import singleChatStore from '@/store/singleChatStore';
import chatsStore from '@/store/chatsStore';
import { useUserAuth } from '@/context/AuthContex';

const Messages = () => {
  const { selectedChat } = chatsStore();
  const { currentUser } = useUserAuth();
  const { chat, fetchSingleChat } = singleChatStore();

  useEffect(() => {
    if (selectedChat) {
      fetchSingleChat(selectedChat.id, currentUser?.id);
    }
  }, [selectedChat, fetchSingleChat, currentUser?.id]);

  if (!chat) return <Typography>Loading messages...</Typography>;

  return (
    <>
      {chat.messages.map((message, index) => (
        <MessageItem key={index} message={message} />
      ))}
      <MessagesBottomScroll chat={chat} />
    </>
  );
};

export default Messages;
