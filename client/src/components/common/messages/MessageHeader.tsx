import Image from 'next/image';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import React from 'react';
import chatsStore from '@/store/chatsStore';
import DefaultImage from '../../../../images/profile/def-avatar.png';

interface Props {
  children: React.ReactNode;
}

const MessageHeader = ({ children }: Props) => {
  const { selectedChat } = chatsStore();

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        p: 2,
        borderBottom: '1px solid #ccc'
      }}
    >
      {selectedChat && (
        <>
          <Image
            src={selectedChat.receiver?.avatar[0] ? selectedChat.receiver.avatar[0] : DefaultImage}
            alt={'Chat receiver image'}
            width={30}
            height={30}
            className={`rounded-5`}
          />

          <Typography sx={{ color: 'text.secondary' }}>
            {selectedChat.receiver ? selectedChat.receiver.username : 'User'}
          </Typography>
        </>
      )}

      {children}
    </Box>
  );
};

export default MessageHeader;
