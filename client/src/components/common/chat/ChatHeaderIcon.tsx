import React, { useEffect } from 'react';
import { IconButton } from '@mui/material';
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import Menu from '@mui/material/Menu';
import useSingleChatStore from '@/store/singleChatStore';

interface Props {
  children: React.ReactNode;
}

const ChatHeaderIcon = ({ children }: Props) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const { openConversation } = useSingleChatStore();

  useEffect(() => {
    if (openConversation) setAnchorEl(null);
  }, [openConversation]);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <>
      <IconButton
        onClick={handleClick}
        size="small"
        aria-controls={open ? 'account-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
      >
        <ChatBubbleOutlineIcon
          fontSize="large"
          className={`bg-primary text-white rounded-5 px-1 ms-2 ms-md-0`}
        />
      </IconButton>

      <Menu
        id="long-menu"
        MenuListProps={{
          'aria-labelledby': 'long-button'
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={() => setAnchorEl(null)}
        sx={{ maxHeight: '500px', width: '250px' }}
      >
        {children}
      </Menu>
    </>
  );
};

export default ChatHeaderIcon;
