import Image from 'next/image';
import DefAvatarImage from '../../../../images/profile/def-avatar.png';
import { Box, Typography } from '@mui/material';
import React from 'react';
import { MultipleChats } from '@/types';

const ChatsView = ({ chat }: { chat: MultipleChats }) => {
  return (
    <>
      <Image
        src={chat.receiver?.avatar[0] ? chat.receiver.avatar[0] : DefAvatarImage}
        alt={`${chat.receiver ? chat.receiver.username : 'Unknown'} image`}
        width={30}
        height={30}
        className={`rounded-5 me-2`}
      />

      <Box>
        <Typography variant="subtitle2">
          {chat.receiver ? chat.receiver.username : 'Unknown'}
        </Typography>

        <Typography variant="caption" display="block" sx={{ fontSize: '8px', color: 'grey' }}>
          message:
        </Typography>

        {chat.lastMessage && (
          <Typography variant="caption" display="block">
            {chat.lastMessage.slice(0, 20)}
            {chat.lastMessage.length > 20 && '...'}
          </Typography>
        )}
      </Box>
    </>
  );
};

export default ChatsView;
