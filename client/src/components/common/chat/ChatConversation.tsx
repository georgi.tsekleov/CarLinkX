import React from 'react';
import Box from '@mui/material/Box';
import styles from './ChatConversation.module.scss';
import Messages from '@/components/common/messages/Messages';
import InputMessage from '@/components/common/messages/InputMessage';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import MessageHeader from '@/components/common/messages/MessageHeader';
import useSocketUpdater from '@/components/common/hooks/useSocketUpdater';
import chatsStore from '@/store/chatsStore';
import { useUserAuth } from '@/context/AuthContex';
import notificationStore from '@/store/notificationStore';
import { updateSeenByChatsDB } from '@/utils/chats';
import useSingleChatStore from '@/store/singleChatStore';

const ChatConversation = () => {
  useSocketUpdater();

  const { currentUser } = useUserAuth();
  const { selectedChat, updateChatsSeenByChatClick } = chatsStore();
  const { number, decreaseNumber } = notificationStore();
  const { openConversation, setOpenConversation } = useSingleChatStore();

  const handleChatOpenMessage = async () => {
    if (currentUser && selectedChat && number > 0) {
      updateChatsSeenByChatClick(currentUser.id);
      selectedChat?.seenBy[0] !== currentUser.id && openConversation && decreaseNumber();

      await updateSeenByChatsDB(selectedChat.id, currentUser.id);
    }
  };

  return (
    openConversation && (
      <Box className={`${styles.Chat} border`} onClick={handleChatOpenMessage}>
        <MessageHeader>
          <IconButton onClick={() => setOpenConversation(false)}>
            <CloseIcon />
          </IconButton>
        </MessageHeader>

        <Box className={`${styles.Conversation}`}>
          <Messages />
        </Box>

        <Box className={`${styles.InputContainer}`}>
          <InputMessage />
        </Box>
      </Box>
    )
  );
};

export default ChatConversation;
