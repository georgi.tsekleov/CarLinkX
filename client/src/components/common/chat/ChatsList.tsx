import React from 'react';
import MenuItem from '@mui/material/MenuItem';
import ChatsView from '@/components/common/chat/ChatsView';
import ChatHeaderIcon from '@/components/common/chat/ChatHeaderIcon';
import { MultipleChats } from '@/types';
import styles from './ChatsList.module.scss';
import { Badge } from '@mui/material';
import { useUserAuth } from '@/context/AuthContex';
import useChatsStore from '@/store/chatsStore';
import notificationStore from '@/store/notificationStore';
import useChatsUpdate from '@/components/common/hooks/useChatsUpdate';
import useChatClick from '@/components/common/hooks/useChatClick';

const ChatsList = () => {
  useChatsUpdate();

  const { currentUser } = useUserAuth();
  const { number } = notificationStore();
  const { chats } = useChatsStore();
  const { handleChatClick } = useChatClick();

  const isChatSeenByCurrentUser = (chat: MultipleChats) =>
    currentUser && chat.seenBy.includes(currentUser?.id);

  return (
    <Badge badgeContent={number} color="error" overlap="circular">
      <ChatHeaderIcon>
        {chats?.map((chat: MultipleChats, idx: number) => (
          <MenuItem
            key={idx}
            onClick={() => handleChatClick(chat)}
            className={`${isChatSeenByCurrentUser(chat) ? '' : styles.UnreadMessage}`}
          >
            <ChatsView chat={chat} />
          </MenuItem>
        ))}
      </ChatHeaderIcon>
    </Badge>
  );
};

export default ChatsList;
