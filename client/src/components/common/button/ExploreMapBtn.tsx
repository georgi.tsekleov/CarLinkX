import React from 'react';
import { Button } from '@mui/material';
import Link from 'next/link';
import SendIcon from '@mui/icons-material/Send';

const ExploreMapBtn = () => {
  return (
    <Link href={`/list`}>
      <Button
        variant="outlined"
        color="secondary"
        endIcon={<SendIcon />}
        className={`text-white bg-primary mt-3`}
        sx={{ fontSize: '20px' }}
      >
        Explore Our Car Rentals
      </Button>
    </Link>
  );
};

export default ExploreMapBtn;
