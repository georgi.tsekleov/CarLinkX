import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import { Button } from '@mui/material';
import React from 'react';

const SearchButton = ({ onClick }: { onClick: () => void }) => {
  return (
    <Button
      variant="outlined"
      color="secondary"
      endIcon={<LocationSearchingIcon />}
      className={`text-white bg-primary`}
      onClick={onClick}
      sx={{
        width: '300px',
        height: '50px',
        fontSize: '20px'
      }}
    >
      Search
    </Button>
  );
};

export default SearchButton;
