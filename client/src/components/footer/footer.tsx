import React from 'react';
import styles from './footer.module.scss';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import { Grid, Typography } from '@mui/material';

const Footer = () => {
  return (
    <footer className={`${styles.Footer} bg-secondary text-white px-4 py-2`}>
      <MaxWidthWrapper>
        <Grid container spacing={2}>
          <Grid item>
            <Typography className={`text-white`}>
              You are a car enthusiast or looking for a convenient way to share your car&apos;s
              location,
              <b>CarLinkX</b> provides the tools and platform for you. Users can share their
              car&apos;s location as a pin on a map, upload images, add information, chat in
              real-time, and process payments. It ensures seamless real-time communication and
              transactions between users.
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Typography className={`text-white`}>
              <b>&copy; {new Date().getFullYear()} CarLingX</b>
            </Typography>
          </Grid>
          <Grid item xs={12} className={'pt-0'}>
            <Typography className={`text-white`}>All rights reserved</Typography>
          </Grid>
        </Grid>
      </MaxWidthWrapper>
    </footer>
  );
};

export default Footer;
