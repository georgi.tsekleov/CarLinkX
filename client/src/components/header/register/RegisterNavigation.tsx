'use client';

import React from 'react';
import SignInBtn from './SignInBtn';
import { useUserAuth } from '@/context/AuthContex';
import UserAvatar from '@/components/common/avatar/UserAvatar';
import Link from 'next/link';
import styles from '@/components/header/MainNavigation.module.scss';
import { Tooltip, styled, TooltipProps, tooltipClasses, Box } from '@mui/material';
import ChatsList from '@/components/common/chat/ChatsList';
import ChatConversation from '@/components/common/chat/ChatConversation';

const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 11
  }
}));

const RegisterNavigation = () => {
  const { currentUser } = useUserAuth();

  return (
    <>
      {currentUser ? (
        <Box className={`custom-flex-center`}>
          <LightTooltip title="Go to profile page">
            <Link href={'/profile'} className={`${styles.Link} me-2`}>
              <UserAvatar currentUser={currentUser} showUploadBtn={false} width={36} height={36} />
            </Link>
          </LightTooltip>

          <ChatsList />
          <ChatConversation />
        </Box>
      ) : (
        <>
          {/*<RegisterBtn />*/}
          <SignInBtn />
        </>
      )}
    </>
  );
};

export default RegisterNavigation;
