import { Button } from '@mui/material';
import styles from '@/components/header/MainNavigation.module.scss';
import React from 'react';

const RegisterBtn = () => {
  const handleRegisterBtn = async () => {
    console.log('click at Sign up');

    try {
      const response = await fetch('/auth/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: 'Gosho1',
          email: 'gosho1@abv.bg',
          password: 'pass1234'
        }),
        credentials: 'include'
      });

      const data = await response.json();
      console.log('Response from server:', data);
    } catch (error) {
      console.error('Register error:', error);
    }
  };
  return (
    <Button className={`${styles.Link}`} onClick={handleRegisterBtn}>
      Register
    </Button>
  );
};

export default RegisterBtn;
