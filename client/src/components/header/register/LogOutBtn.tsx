import styles from '@/components/header/MainNavigation.module.scss';
import { Button } from '@mui/material';
import React, { useState } from 'react';
import { useUserAuth } from '@/context/AuthContex';
import Logout from '@mui/icons-material/Logout';
import CircularProgress from '@mui/material/CircularProgress';
import { navigateTo } from '@/utils/navigateTo';

const LogOutBtn = () => {
  const { updateUser } = useUserAuth();
  const [loading, setLoading] = useState(false);

  const handleLogOutBtn = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setLoading(true);

    try {
      await fetch('api/auth/logout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include'
      });

      updateUser(null);
      navigateTo('/');
    } catch (error) {
      console.error('Sign in error:', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Button
      className={`${styles.Link} bg-primary p-2 text-white rounded-2`}
      onClick={handleLogOutBtn}
      endIcon={
        !loading ? (
          <Logout fontSize="small" />
        ) : (
          <CircularProgress size={20} className={`text-white`} />
        )
      }
      disabled={loading}
    >
      Log out
    </Button>
  );
};

export default LogOutBtn;
