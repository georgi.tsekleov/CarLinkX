import { Button, TextField, Typography, Box, Dialog } from '@mui/material';
import React, { ChangeEvent, useState } from 'react';
import { useUserAuth } from '@/context/AuthContex';
import LoginIcon from '@mui/icons-material/Login';
import CircularProgress from '@mui/material/CircularProgress';
import InputIcon from '@mui/icons-material/Input';

const initialFormState = {
  username: '',
  password: ''
};

const SignInBtn = () => {
  const [open, setOpen] = useState(false);
  const [formData, setFormData] = useState(initialFormState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const { updateUser } = useUserAuth();

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSignIn = async () => {
    console.log('Sign In button clicked with data:', formData);
    setLoading(true);
    setError(false);

    try {
      const response = await fetch('/api/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData),
        credentials: 'include'
      });

      if (!response.ok) {
        setError(true);
      }

      const data = await response.json();
      console.log('Response login from server:', data);
      updateUser(data.user);
    } catch (error) {
      console.error('Sign in error:', error);
      setError(true);
    }

    setFormData(initialFormState);
    setLoading(false);
  };

  const isFormFilled = formData.username.trim().length > 0 && formData.password.trim().length > 0;

  return (
    <>
      <Button className={'rounded-5'}>
        <LoginIcon
          className={`bg-primary text-white rounded-5 rounded-md-2 px-2`}
          onClick={() => setOpen(true)}
          fontSize="large"
        />
      </Button>

      <Dialog
        onClose={() => {
          setOpen(false);
          setFormData(initialFormState);
        }}
        open={open}
      >
        <Box className={`px-4 pt-5 pb-4 custom-flex-center flex-column`}>
          <Typography variant="h6">Form</Typography>
          <TextField
            label="Username"
            name="username"
            value={formData.username}
            onChange={handleChange}
            fullWidth
            margin="normal"
            variant="standard"
          />
          <TextField
            label="Password"
            name="password"
            type="password"
            value={formData.password}
            onChange={handleChange}
            fullWidth
            margin="normal"
            variant="standard"
          />
          <Button
            className={`mt-4 ${!loading && !isFormFilled ? 'bg-secondary' : 'bg-primary text-white'}`}
            onClick={handleSignIn}
            variant="contained"
            endIcon={
              !loading ? <InputIcon /> : <CircularProgress size={20} className={'text-white'} />
            }
            disabled={loading || !isFormFilled}
          >
            Sign In
          </Button>
          {error && (
            <Typography className={`mt-4 text-danger text-center`} variant="h6">
              Sign in fail. Try again!
            </Typography>
          )}
        </Box>
      </Dialog>
    </>
  );
};

export default SignInBtn;
