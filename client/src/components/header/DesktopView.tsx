import MainNavigation from '@/components/header/MainNavigation';
import RegisterNavigation from '@/components/header/register/RegisterNavigation';
import React from 'react';

const DesktopView = () => {
  return (
    <>
      <div className={`d-none d-sm-block ps-3`}>
        <MainNavigation />
      </div>

      <div className={`d-none d-sm-block pe-3`}>
        <RegisterNavigation />
      </div>
    </>
  );
};

export default DesktopView;
