import React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import LinkLogo from '@/components/header/LinkLogo';
import SideDrawer from '@/components/header/SideDrawer';
import MainNavigation from '@/components/header/MainNavigation';
import { Box } from '@mui/material';
import RegisterNavigation from '@/components/header/register/RegisterNavigation';
import MenuOpenIcon from '@mui/icons-material/MenuOpen';

const MobileView = () => {
  return (
    <Container maxWidth="xs" className={`d-block d-sm-none w-100`}>
      <Grid container alignItems="center" spacing={4}>
        <Grid item xs={6} sx={{ textAlign: 'left' }}>
          <LinkLogo />
        </Grid>

        <Grid
          item
          xs={6}
          sx={{
            textAlign: 'right',
            pr: 0,
            display: 'flex',
            justifyContent: 'end',
            alignItems: 'center'
          }}
        >
          <Box>
            <RegisterNavigation />
          </Box>

          <SideDrawer
            sideDrawer={'right'}
            icon={
              <MenuOpenIcon fontSize="large" className={`bg-primary text-white rounded-5 px-1`} />
            }
          >
            <MainNavigation />
          </SideDrawer>
        </Grid>
      </Grid>
    </Container>
  );
};

export default MobileView;
