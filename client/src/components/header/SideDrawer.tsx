'use client';

import React, { useState } from 'react';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { Box } from '@mui/material';

type Anchor = 'top' | 'left' | 'bottom' | 'right';

interface Props {
  children: React.ReactNode;
  icon: React.ReactNode;
  sideDrawer: Anchor;
}

const SideDrawer = ({ children, icon, sideDrawer }: Props) => {
  const [state, setState] = useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  return (
    <Box>
      <Button className="py-0" onClick={toggleDrawer(sideDrawer, true)}>
        {icon}
      </Button>

      <Drawer
        anchor={sideDrawer}
        open={state[sideDrawer]}
        onClose={toggleDrawer(sideDrawer, false)}
      >
        <Button onClick={toggleDrawer(sideDrawer, false)}>
          <HighlightOffIcon fontSize="large" className="mt-4 text-dark" />
        </Button>

        <Box className="px-sm-5 fs-3" onClick={toggleDrawer(sideDrawer, false)}>
          {children}
        </Box>
      </Drawer>
    </Box>
  );
};

export default SideDrawer;
