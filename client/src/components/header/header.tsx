import React from 'react';
import styles from './header.module.scss';
import MaxWidthWrapper from '../MaxWidthWrapper';
import MobileView from '@/components/header/MobileView';
import DesktopView from './DesktopView';

export default function Header() {
  return (
    <nav className={`${styles.Header} text-center py-3`}>
      <MaxWidthWrapper
        sx={{ justifyContent: 'space-between', alignItems: 'center' }}
        className={`${styles.HeaderNavigation} `}
      >
        <MobileView />
        <DesktopView />
      </MaxWidthWrapper>
    </nav>
  );
}
