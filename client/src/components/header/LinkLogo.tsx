import styles from '@/components/header/MainNavigation.module.scss';
import Link from 'next/link';
import React from 'react';

const LinkLogo = () => {
  return (
    <Link href={'/'} className={`${styles.LinkLogo} m-0 d-flex align-items-center`}>
      <div>
        <span className={`fs-5`}>
          <i>CarLink</i>
        </span>
      </div>

      <div>
        <span>
          <b className={`fs-1 text-primary`}>X</b>
        </span>
      </div>
    </Link>
  );
};
export default LinkLogo;
