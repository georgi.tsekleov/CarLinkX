import Box from '@mui/material/Box';
import React from 'react';
import Link from 'next/link';
import styles from './MainNavigation.module.scss';
import LinkLogo from '@/components/header/LinkLogo';

const MainNavigation = () => {
  return (
    <Box className={`${styles.MainNavigation} d-flex align-items-center`}>
      <div className={`d-none d-sm-block me-3`}>
        <LinkLogo />
      </div>
      <Link href={'/'} className={`${styles.Link}`}>
        <span>Home</span>
      </Link>
      <Link href={'/about'} className={`${styles.Link}`}>
        <span>About</span>
      </Link>
      <Link href={'/contacts'} className={`${styles.Link}`}>
        <span>Contacts</span>
      </Link>
      <Link href={'/people'} className={`${styles.Link}`}>
        <span>People</span>
      </Link>
      <Link href={'/list'} className={`${styles.Link}`}>
        <span>Map</span>
      </Link>
      <Link href={'/profile'} className={`${styles.Link}`}>
        <span>Profile</span>
      </Link>
      <Link href={'/new-post'} className={`${styles.Link} p-2 m-3 bg-primary text-white rounded-2`}>
        <span>Create New Post</span>
      </Link>
    </Box>
  );
};
export default MainNavigation;
