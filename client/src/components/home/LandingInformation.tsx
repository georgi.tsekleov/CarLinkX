import CustomContainer from '@/components/common/gridContainer/CustomContainer';
import CustomCol from '@/components/common/gridContainer/CustomCol';
import Car from '../../../images/home/locations-car-sm.png';
import Image from 'next/image';
import styles from './LandingInformation.module.scss';
import Statistics from '@/components/home/Statistics';
import CustomRow from '../common/gridContainer/CustomRow';
import ExploreMapBtn from '@/components/common/button/ExploreMapBtn';

const LandingInformation = () => {
  return (
    <CustomContainer>
      <CustomRow>
        <CustomCol xs={12} sm={6} className={`p-0 mb-3`}>
          <Image
            src={Car}
            alt={`Map locations of cars`}
            priority
            className={`${styles.CarImg}`}
          ></Image>
        </CustomCol>

        <CustomCol xs={12} sm={6} className={`custom-flex-center flex-column p-0`}>
          <h2>Provides seamless integration with your vehicle</h2>
          <p>
            Offering real-time diagnostics, remote control features, and enhanced safety measures.
            Experience the convenience and peace of mind that comes with Car Link X.
          </p>
          <Statistics />

          <ExploreMapBtn />
        </CustomCol>
      </CustomRow>
    </CustomContainer>
  );
};

export default LandingInformation;
