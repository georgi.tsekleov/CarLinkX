import CustomContainer from '@/components/common/gridContainer/CustomContainer';
import CustomCol from '@/components/common/gridContainer/CustomCol';
import CustomRow from '@/components/common/gridContainer/CustomRow';

const StatisticItem = ({ value, label }: { value: string; label: string }) => {
  return (
    <CustomCol xs={12} sm={4} className="custom-flex-center flex-column p-0">
      <p className="m-0 fs-2 text-start">{value}</p>
      <p className="m-0">{label}</p>
    </CustomCol>
  );
};

const Statistics = () => {
  return (
    <CustomContainer className={`mt-3`}>
      <CustomRow>
        <StatisticItem value="10+" label="Years of experience" />
        <StatisticItem value="1000+" label="Happy clients" />
        <StatisticItem value="2000+" label="Cars ready" />
      </CustomRow>
    </CustomContainer>
  );
};

export default Statistics;
