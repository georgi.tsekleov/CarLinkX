import { SelectChangeEvent } from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';

interface Props {
  region: string;
  onChange: (event: SelectChangeEvent) => void;
}

const RegionSelector = ({ region, onChange }: Props) => {
  return (
    <FormControl sx={{ m: 1, minWidth: 120 }}>
      <FormLabel id="demo-row-radio-buttons-group-label" className={`text-center fs-4 text-dark`}>
        Region
      </FormLabel>
      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
        onChange={onChange}
        value={region}
      >
        <FormControlLabel className={`mx-1`} value="Sofia" control={<Radio />} label="Sofia" />
        <FormControlLabel className={`mx-1`} value="Plovdiv" control={<Radio />} label="Plovdiv" />
        <FormControlLabel className={`mx-1`} value="Burgas" control={<Radio />} label="Burgas" />
      </RadioGroup>
    </FormControl>
  );
};
export default RegionSelector;
