import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { Typography } from '@mui/material';

function valuetext(value: number) {
  return `${value}$`;
}

const minDistance = 10;

interface Props {
  value: number[];
  onChange: (value: number[]) => void;
}

export default function PriceRangeSlider({ value, onChange }: Props) {
  // const [value, setValue] = useState<number[]>([33, 100]);

  const handleChange = (event: Event, newValue: number | number[], activeThumb: number) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    let newPrice = [...value];
    if (activeThumb === 0) {
      newPrice = [Math.min(newValue[0], value[1] - minDistance), value[1]];
    } else {
      newPrice = [value[0], Math.max(newValue[1], value[0] + minDistance)];
    }

    onChange(newPrice);
  };

  const marks = [
    {
      value: value[0],
      label: `${value[0]}$`
    },
    {
      value: value[1],
      label: `${value[1]}$`
    }
  ];

  return (
    <Box sx={{ width: 300 }}>
      <Typography variant="body2" className={`text-center fs-4`}>
        Price range
      </Typography>
      <Slider
        getAriaLabel={() => 'Minimum distance'}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        getAriaValueText={valuetext}
        disableSwap
        marks={marks}
        // color="primary"
        className={`text-primary`}
      />
    </Box>
  );
}
