'use client';

import React from 'react';
import { SelectChangeEvent } from '@mui/material/Select';
import PriceRangeSlider from '@/components/home/PriceRangeSlider';
import CustomContainer from '../common/gridContainer/CustomContainer';
import CustomCol from '../common/gridContainer/CustomCol';
import RegionSelector from '@/components/home/RegionSelector';
import CustomRow from '@/components/common/gridContainer/CustomRow';
import { SelectionState } from '@/app/list/types';

interface Props {
  onChange: (state: SelectionState) => void;
  selections: SelectionState;
}

const Selections = ({ onChange, selections }: Props) => {
  const handleRegionChange = (event: SelectChangeEvent) => {
    const newState = {
      ...selections,
      region: event.target.value
    };
    onChange(newState);
  };

  const handlePriceChange = (newPrice: number[]) => {
    const newState = {
      ...selections,
      price: newPrice
    };
    onChange(newState);
  };

  return (
    <>
      <CustomContainer className={`me-0 me-md-3 p-0 mt-4`}>
        <CustomRow>
          <CustomCol xs={12} className={`custom-flex-center flex-column p-0 mt-3`}>
            <RegionSelector region={selections.region} onChange={handleRegionChange} />
          </CustomCol>

          <CustomCol xs={12} className={`custom-flex-center flex-column p-0 mt-3`}>
            <PriceRangeSlider value={selections.price} onChange={handlePriceChange} />
          </CustomCol>
        </CustomRow>
      </CustomContainer>
    </>
  );
};

export default Selections;
