import { Button, CircularProgress } from '@mui/material';
import React, { createContext, useEffect, useState } from 'react';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import { UploadedWidgetImg } from '@/types';

interface CloudinaryScriptContextType {
  loaded: boolean;
}

const CloudinaryScriptContext = createContext<CloudinaryScriptContextType>({
  loaded: false
});

interface UploadWidgetProps {
  uwConfig: {
    cloudName: string;
    uploadPreset: string;
    multiple?: boolean;
    maxImageFileSize?: number;
    folder?: string;
    [key: string]: any;
  };
  setState: (urls: UploadedWidgetImg[]) => void;
  showUploadBtn: boolean;
}

const UploadWidget = ({ uwConfig, setState, showUploadBtn }: UploadWidgetProps) => {
  const [loaded, setLoaded] = useState(false);
  const [uploaded, setUploaded] = useState(false);
  const [uploadedImages, setUploadedImages] = useState<UploadedWidgetImg[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!loaded) {
      const uwScript = document.getElementById('uw');
      if (!uwScript) {
        const script = document.createElement('script');
        script.setAttribute('async', '');
        script.setAttribute('id', 'uw');
        script.src = 'https://upload-widget.cloudinary.com/global/all.js';
        script.addEventListener('load', () => setLoaded(true));
        document.body.appendChild(script);
      } else {
        setLoaded(true);
      }
    }
  }, [loaded]);

  const initializeCloudinaryWidget = () => {
    if (loaded) {
      setLoading(true);

      // @ts-ignore
      let myWidget = window.cloudinary.createUploadWidget(uwConfig, (error: any, result: any) => {
        if (!error && result && result.event === 'success') {
          const newUploadedImages = [...uploadedImages, result.info.secure_url];

          setUploadedImages(newUploadedImages);
          setState(newUploadedImages);
        }
        setLoading(false);
      });

      myWidget.open();
      setUploaded(true);
    }
  };

  return (
    <CloudinaryScriptContext.Provider value={{ loaded }}>
      {showUploadBtn && (
        <Button
          variant="outlined"
          color="secondary"
          id="upload_widget"
          className={`cloudinary-button d-flex align-items-center bg-primary rounded-2`}
          onClick={initializeCloudinaryWidget}
          disabled={loading}
        >
          <span className={`me-2`}>Upload Image{uwConfig.multiple === true ? 's' : ''}</span>
          {loading ? <CircularProgress color="inherit" size={20} /> : <FileUploadIcon />}
        </Button>
      )}
    </CloudinaryScriptContext.Provider>
  );
};

export default UploadWidget;
