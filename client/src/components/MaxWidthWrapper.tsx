import styles from './MaxWidthWrapper.module.scss';
import { Box, SxProps } from '@mui/material';
import React from 'react';
import clsx from 'clsx';

interface MaxWidthWrapperProps {
  children: React.ReactNode;
  sx?: SxProps;
  className?: string;

  [x: string]: any;
}

const MaxWidthWrapper: React.FC<MaxWidthWrapperProps> = ({
  children,
  sx,
  className,
  ...otherProps
}) => {
  return (
    <Box
      component="section"
      className={clsx(styles.MaxWidthWrapper, className)}
      sx={sx}
      {...otherProps}
    >
      {children}
    </Box>
  );
};

export default MaxWidthWrapper;
