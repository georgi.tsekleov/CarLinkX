import { create } from 'zustand';

type Store = {
  number: number;
  fetchNotification: (currentUserId: string) => void;
  resetNumber: () => void;
  increaseNumber: () => void;
  decreaseNumber: () => void;
};

const notificationStore = create<Store>((set) => ({
  number: 0,
  fetchNotification: async (currentUserId: string) => {
    const data = await fetch(`/api/notifications`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ tokenUserId: currentUserId }),
      credentials: 'include'
    });
    const res = await data.json();

    set({ number: res });
  },
  resetNumber: () => set({ number: 0 }),
  increaseNumber: () => set((state) => ({ number: state.number + 1 })),
  decreaseNumber: () => set((state) => ({ number: state.number - 1 }))
}));
export default notificationStore;
