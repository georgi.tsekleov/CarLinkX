import { create } from 'zustand';
import { SingleChat, Message } from '@/types';

type SingleChatStore = {
  chat: SingleChat | null;
  loading: boolean;
  error: string | null;
  fetchSingleChat: (chatId: string | null, currentUserId: string | undefined) => void;
  updateChat: (newDataMessage: Message) => void;
  openConversation: boolean;
  setOpenConversation: (isOpen: boolean) => void;
};

const useSingleChatStore = create<SingleChatStore>((set) => ({
  chat: null,
  loading: false,
  error: null,
  openConversation: false,

  fetchSingleChat: async (chatId: string | null, currentUserId: string | undefined) => {
    set({ loading: true });

    try {
      const response = await fetch(`/api/chats/get-chat`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ chatId: chatId, tokenUserId: currentUserId }),
        credentials: 'include'
      });

      if (!response.ok) console.error('Failed to fetch chat');

      const data: SingleChat = await response.json();
      set({ chat: data, loading: false, error: null });
    } catch (error: any) {
      set({ error: error.message, loading: false });
    }
  },

  updateChat: (newDataMessage: Message) => {
    set((state) => ({
      chat: state.chat
        ? { ...state.chat, messages: [...state.chat.messages, newDataMessage] }
        : null
    }));
  },

  setOpenConversation: (isOpen: boolean) => {
    set({ openConversation: isOpen });
  }
}));

export default useSingleChatStore;
