import { create } from 'zustand';
import { MultipleChats, Message, User } from '@/types';
import { LOCAL_HOST_API } from '../../state';

type MultipleChatsStore = {
  chats: MultipleChats[] | null;
  loading: boolean;
  error: string | null;
  selectedChat: MultipleChats | null;
  fetchChats: (userId: string) => void;
  updateChatsSeenBy: (data: Message) => void;
  updateChatsLastMessage: (data: Message) => void;
  setSelectedChat: (chat: MultipleChats) => void;
  updateChatsSeenByChatClick: (userId: string) => void;
};

const useChatsStore = create<MultipleChatsStore>((set) => ({
  chats: null,
  loading: false,
  error: null,
  selectedChat: null,

  fetchChats: async (currentUserId) => {
    set({ loading: true });
    try {
      const response = await fetch(`/api/chats/get-chats`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ userId: currentUserId }),
        credentials: 'include'
      });

      if (!response.ok) console.error('Failed to fetch chats');

      const data: MultipleChats[] = await response.json();
      set({ chats: data, loading: false, error: null });
    } catch (error: any) {
      set({ error: error.message, loading: false });
    }
  },

  updateChatsSeenBy: (data) => {
    set((state) => ({
      chats:
        state.chats?.map((chat) =>
          chat.id === data.chatId ? { ...chat, seenBy: [data.userId] } : chat
        ) || null
    }));
  },

  updateChatsSeenByChatClick: (userId: string) => {
    set((state) => ({
      chats:
        state.chats?.map((chat) =>
          chat.id === state.selectedChat?.id ? { ...chat, seenBy: [userId] } : chat
        ) || null
    }));
  },

  updateChatsLastMessage: (data) => {
    set((state) => ({
      chats: state.chats?.map((chat) =>
        chat.id === data.chatId ? { ...chat, lastMessage: data.text } : chat
      )
    }));
  },

  setSelectedChat: (chat: MultipleChats) => {
    console.log('selected chat', chat);

    set({ selectedChat: chat });
  }
}));

export default useChatsStore;
