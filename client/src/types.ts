import { StaticImageData } from 'next/image';
import { StaticImport } from 'next/dist/shared/lib/get-img-props';

export type UploadedWidgetImg = string | StaticImageData | undefined;

export interface User {
  avatar: StaticImageData[];
  createdAt: string;
  id: string;
  username: string;
  email: string;
}

type AvatarImage = string | StaticImport;

interface Receiver {
  id: string;
  username: string;
  avatar: AvatarImage[];
}

export interface MultipleChats {
  id: string;
  createdAt: string;
  lastMessage: string;
  receiver?: Receiver;
  seenBy: string[];
  userIDs: string[];
}

export interface SingleChat {
  id: string;
  createdAt: string;
  lastMessage: string;
  seenBy: string[];
  userIDs: string[];
  messages: Message[];
}

export interface Message {
  chatId: string;
  createdAt: string;
  id: string;
  text: string;
  userId: string;
}
