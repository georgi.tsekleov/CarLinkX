'use client';

import React, { createContext, useContext, useEffect, useState } from 'react';
import { io, Socket } from 'socket.io-client';
import { useUserAuth } from '@/context/AuthContex';

const SocketContext = createContext<Socket | null>(null);

export const useSocketContext = () => {
  return useContext(SocketContext);
};

export const SocketContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [socket, setSocket] = useState<Socket | null>(null);
  const { currentUser } = useUserAuth();

  useEffect(() => {
    const socketInstance = io('https://hungry-tortoiseshell-friction.glitch.me');

    setSocket(socketInstance);
    return () => {
      socketInstance.disconnect();
    };
  }, []);

  useEffect(() => {
    if (currentUser) {
      socket?.emit('newUser', currentUser.id);
    }
  }, [currentUser, socket]);

  return <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>;
};
