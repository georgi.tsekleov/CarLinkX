'use client';

import React, { createContext, useContext, useEffect, useState } from 'react';
import { User } from '@/types';

interface AuthContextType {
  currentUser: User | null;
  updateUser: (data: User | null) => void;
}

const AuthContext = createContext<AuthContextType>({
  currentUser: null,
  updateUser: () => {}
});

export const useUserAuth = () => {
  return useContext(AuthContext);
};

export const AuthContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [currentUser, setCurrentUser] = useState<User | null>(null);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedUser = localStorage.getItem('user');
      if (storedUser && storedUser !== 'undefined') {
        setCurrentUser(JSON.parse(storedUser));
      }
    }
  }, []);

  const updateUser = (data: User | null) => {
    setCurrentUser(data);
    if (typeof window !== 'undefined') {
      localStorage.setItem('user', JSON.stringify(data));
    }
  };

  return (
    <AuthContext.Provider value={{ currentUser, updateUser }}>{children}</AuthContext.Provider>
  );
};
