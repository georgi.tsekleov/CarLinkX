export const updateSeenByChatsDB = async (chatId: string, currentUserId: string | null) => {
  try {
    const response = await fetch(`/api/chats/read-chat`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({ chatId, currentUserId }),
      credentials: 'include'
    });

    if (!response.ok) {
      console.error('Error on response to update seenBy chat');
    }

    const updatedChat = await response.json();
    console.log('Updated chat:', updatedChat);
  } catch (e) {
    console.error('Error on update chat ', e);
  }
};
