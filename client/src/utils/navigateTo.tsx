export const navigateTo = (page?: string) => {
  window.location.href = page || '/';
};
