'use client';
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#239eab'
    },
    secondary: {
      main: '#d9d9d9'
    }
  },
  typography: {
    fontFamily: ['JetBrains Mono', 'IBM Plex Sans', 'Exo 2', 'Arial'].join(',')
  }
});

export default theme;
