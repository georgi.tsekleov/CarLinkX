'use client';

import React from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import styles from './MapComponent.module.scss';
import myLocation from '../../../images/location/me_location.png';
import carLocation from '../../../images/location/car_location.png';
import Image from 'next/image';
import { Box, Typography } from '@mui/material';
import { Post } from '@/app/types';
import DynamicLinkBtn from '@/app/list/DynamicLinkBtn';
import PaidIcon from '@mui/icons-material/Paid';
import LocationOnIcon from '@mui/icons-material/LocationOn';

const myMarkerIcon = L.icon({
  iconUrl: myLocation.src,
  iconSize: [42, 42],
  iconAnchor: [16, 32]
});

const carsMarkerIcon = L.icon({
  iconUrl: carLocation.src,
  iconSize: [42, 42],
  iconAnchor: [16, 32]
});

const CustomMapView = ({ carsList }: { carsList: Post[] }) => {
  return (
    <MapContainer
      center={[42.697347, 23.345365]}
      zoom={11}
      scrollWheelZoom={false}
      className={`${styles.Map} rounded-3 border border-5`}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker icon={myMarkerIcon} position={[42.697347, 23.345365]}>
        <Popup>Me</Popup>
      </Marker>

      {carsList.map((car, idx) => (
        <Marker
          key={car.id}
          icon={carsMarkerIcon}
          position={[Number(car.latitude), Number(car.longitude)]}
        >
          <Popup>
            <Box className={`custom-flex-center`}>
              <Image
                src={car.images[0]}
                alt={car.title}
                height={100}
                width={150}
                className={`rounded-2 mb-2`}
              />
              <DynamicLinkBtn carId={car.id} />
            </Box>

            <Box>
              <Typography variant="h5" className={`text-start`}>
                {car.title}
              </Typography>
              <Typography variant="body2" className={`text-start`}>
                <PaidIcon /> {car.price}/day
              </Typography>
              <Typography variant="body2" className={`text-start mb-5`}>
                <LocationOnIcon />
                {car.address}, {car.city}
              </Typography>
              <DynamicLinkBtn carId={car.id} />
            </Box>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};

export default CustomMapView;
