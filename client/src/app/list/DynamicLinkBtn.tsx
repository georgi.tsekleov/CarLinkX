'use client';

import { Button, Link } from '@mui/material';
import React from 'react';
import ReadMoreIcon from '@mui/icons-material/ReadMore';

const DynamicLinkBtn = ({ carId }: { carId: string | undefined }) => {
  return (
    <Link
      href={`/list/${carId}`}
      sx={{ textDecoration: 'none' }}
      className={`position-absolute bottom-0 end-0 m-2`}
    >
      <Button
        variant="contained"
        color="primary"
        size="small"
        sx={{ mt: 2 }}
        endIcon={<ReadMoreIcon />}
      >
        More
      </Button>
    </Link>
  );
};

export default DynamicLinkBtn;
