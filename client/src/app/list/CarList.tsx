import React from 'react';
import { Card, CardContent, Typography, Box } from '@mui/material';
import DynamicLinkBtn from './DynamicLinkBtn';
import { Post } from '@/app/types';
import PaidIcon from '@mui/icons-material/Paid';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import DateRangeIcon from '@mui/icons-material/DateRange';

const CarList = ({ carsList }: { carsList: Post[] }) => {
  return carsList.length === 0 ? (
    <Box className={`custom-flex-center mt-2`}>
      <Typography variant="h5">There is no data!</Typography>
    </Box>
  ) : (
    <Box
      className={`mt-3 p-2 border rounded-2  bg-primary`}
      sx={{ maxHeight: '20rem', overflow: 'scroll' }}
    >
      {carsList?.map((car) => (
        <Card key={car.id} sx={{ marginBottom: 2 }} className={`position-relative`}>
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {car.title}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              className={`position-absolute top-0 end-0 p-2 d-flex align-items-center `}
            >
              <PaidIcon />
              {car.price}/day
            </Typography>
            <Typography variant="body2" color="text.secondary" className={`mb-3`}>
              {car.description.slice(0, 150)}...
            </Typography>

            <Typography
              variant="body2"
              color="text.secondary"
              className={`position-absolute bottom-0 start-0 p-1 d-flex align-items-center`}
            >
              {/*Format date*/}
              <DateRangeIcon />
              {car?.createdAt && car.createdAt.split('.')[0].split('T')[0]} <AccessTimeIcon />
              {car?.createdAt && car.createdAt.split('.')[0].split('T')[1]}
            </Typography>

            <DynamicLinkBtn carId={car.id} />
          </CardContent>
        </Card>
      ))}
    </Box>
  );
};

export default CarList;
