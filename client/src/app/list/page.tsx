'use client';

import React, { useEffect, useState } from 'react';
import CustomContainer from '@/components/common/gridContainer/CustomContainer';
import CustomCol from '@/components/common/gridContainer/CustomCol';
import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import Selections from '@/components/home/Selections';
import CarList from './CarList';
import DynamicMapComponent from '@/app/list/DynamicMapComponent';
import CustomRow from '@/components/common/gridContainer/CustomRow';
import { Post } from '@/app/types';
import { LOCAL_HOST_API } from '../../../state';
import { SelectionState } from '@/app/list/types';
import useCarListSelections from '@/components/common/hooks/useCarListSelections';

const Page = () => {
  const [carsList, setCarsList] = useState<Post[]>([]);
  const { filteredCarsList, selections, setSelections } = useCarListSelections(carsList);

  useEffect(() => {
    const fetchCarPosts = async () => {
      try {
        const response = await fetch(`/api/posts`, { method: 'GET' });
        const data = await response.json();
        setCarsList(data);
      } catch (error) {
        console.error('Error fetching list page:', error);
        setCarsList([]);
      }
    };

    fetchCarPosts();
  }, [selections]);

  const handleSelectionsChange = (newSelections: SelectionState) => {
    setSelections(newSelections);
  };

  return (
    <SectionMUI className={`px-4`}>
      <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
        <h1 className={`text-center gradient-primary-text`}> Car Rentals and Locations</h1>

        <CustomContainer className={`me-0 me-md-3 p-0 mt-4`}>
          <CustomRow>
            <CustomCol xs={12} md={6} className={`p-3`}>
              <Selections onChange={handleSelectionsChange} selections={selections} />
              <CarList carsList={filteredCarsList} />
            </CustomCol>

            <CustomCol xs={12} md={6} className={`p-3`}>
              <DynamicMapComponent carsList={filteredCarsList} />
            </CustomCol>
          </CustomRow>
        </CustomContainer>
      </MaxWidthWrapper>
    </SectionMUI>
  );
};

export default Page;
