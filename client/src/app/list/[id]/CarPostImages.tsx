import React, { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import Image from 'next/image';
import styles from './CarPostImages.module.scss';

const CarPostImages = ({ images }: { images?: string[] }) => {
  const [selectedImage, setSelectedImage] = useState<string | undefined>(
    images ? images[0] : undefined
  );

  const handleImageClick = (image: string) => {
    setSelectedImage(image);
  };

  useEffect(() => {
    if (images) {
      setSelectedImage(images[0]);
    }
  }, [images]);

  return (
    <Box className={`custom-flex-center flex-column `}>
      {selectedImage && (
        <Box mt={2} display="flex" justifyContent="center" className={`d-none d-md-block`}>
          <Image
            src={selectedImage}
            alt="Selected Image"
            width={500}
            height={350}
            priority
            style={{ cursor: 'pointer', borderRadius: '8px' }}
            className={`${styles.CarPostImage}`}
          />
        </Box>
      )}

      <Box
        mt={2}
        display="flex"
        flexWrap="wrap"
        gap={2}
        justifyContent="center"
        sx={{ overflowX: 'auto' }}
      >
        {images?.map((image, idx) => (
          <Box key={idx} onClick={() => handleImageClick(image)} sx={{ cursor: 'pointer' }}>
            <Image
              src={image}
              alt={`Image ${idx + 1}`}
              width={120}
              height={80}
              priority
              style={{ borderRadius: '8px' }}
              className={`${styles.CarPostSmallImages}`}
            />
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default CarPostImages;
