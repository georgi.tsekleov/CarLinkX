'use client';

import React, { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import { Post } from '@/app/types';
import CarPostImages from '@/app/list/[id]/CarPostImages';
import DynamicMapComponent from '@/app/list/DynamicMapComponent';
import CustomContainer from '@/components/common/gridContainer/CustomContainer';
import CustomCol from '@/components/common/gridContainer/CustomCol';
import CustomRow from '@/components/common/gridContainer/CustomRow';
import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import CarInformation from '@/app/list/[id]/CarInformation';

const Page = ({ params }: { params: { id: string } }) => {
  const [carPost, setCarPost] = useState<Post>();
  useEffect(() => {
    const fetchCarPosts = async () => {
      try {
        const response = await fetch('/api/post', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ id: params.id })
        });

        const data = await response.json();
        setCarPost(data);
      } catch (error) {
        console.error('Error fetching dynamic list page:', error);
      }
    };

    fetchCarPosts();
  }, [params.id]);

  return (
    <SectionMUI className={`px-4`}>
      <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
        <CustomContainer className={`me-0 me-md-3 p-0`}>
          <CustomRow>
            <CustomCol xs={12} sm={6} className={`mb-3`}>
              <CarInformation carPost={carPost} />
            </CustomCol>

            <CustomCol xs={12} sm={6} className={`mb-3  pe-0 pe-md-3`}>
              <CarPostImages images={carPost?.images} />
            </CustomCol>
          </CustomRow>

          <CustomRow>
            <CustomCol xs={12} className={`mt-3`}>
              {carPost && (
                <Box sx={{ width: '100%', height: '30rem' }}>
                  <DynamicMapComponent carsList={[carPost]} />
                </Box>
              )}
            </CustomCol>
          </CustomRow>
        </CustomContainer>
      </MaxWidthWrapper>
    </SectionMUI>
  );
};
export default Page;
