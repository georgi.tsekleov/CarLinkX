import { Box, CardContent, Typography } from '@mui/material';
import React from 'react';
import { Post, VehicleType } from '@/app/types';
import Image from 'next/image';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import AddRoadIcon from '@mui/icons-material/AddRoad';
import MultipleStopIcon from '@mui/icons-material/MultipleStop';
import PaidIcon from '@mui/icons-material/Paid';
import LoadingSpinner from '@/components/loading/LoadingSpinner';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import AirportShuttleIcon from '@mui/icons-material/AirportShuttle';
import DirectionsBusIcon from '@mui/icons-material/DirectionsBus';
import TwoWheelerIcon from '@mui/icons-material/TwoWheeler';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import DateRangeIcon from '@mui/icons-material/DateRange';
import SendMessage from '@/components/common/messages/SendMessage';

const vehicleIcons = new Map([
  ['car', <DirectionsCarIcon key="car" sx={{ fontSize: 60 }} />],
  ['motorcycle', <TwoWheelerIcon key="motorcycle" sx={{ fontSize: 45 }} />],
  ['van', <AirportShuttleIcon key="van" sx={{ fontSize: 55 }} />],
  ['bus', <DirectionsBusIcon key="bus" sx={{ fontSize: 50 }} />]
]);

const handleVehicleIcon = (vehicle: VehicleType) => {
  return vehicleIcons.get(vehicle) || null;
};

const CarInformation = ({ carPost }: { carPost?: Post }) => {
  return carPost ? (
    <CardContent className={`mt-3 p-0 position-relative pe-2`}>
      <Box className={`position-absolute top-0 end-0 p-1 text-end`}>
        {handleVehicleIcon(carPost?.vehicle)}

        <Typography variant="body2" className={`d-flex align-items-center justify-content-end`}>
          <PaidIcon /> {carPost?.price}/day
        </Typography>
      </Box>

      {carPost?.user.avatar[0] && (
        <Box>
          <Image src={carPost.user.avatar[0]} alt={'User post'} width={50} height={50} />

          <Typography variant="body2" color="text.secondary">
            {carPost?.user.username}
          </Typography>
        </Box>
      )}

      <SendMessage receiverId={carPost.userId} />

      <Typography gutterBottom variant="h1" component="div" className={`fs-2`}>
        {carPost?.title}
      </Typography>

      <Typography variant="subtitle1" color="text.secondary" className={`my-4`}>
        {carPost?.description}
      </Typography>

      <Typography variant="body2" className={`d-flex align-items-center mb-2`}>
        <LocationOnIcon /> {carPost?.address}, {carPost?.city}
      </Typography>

      <Typography variant="body2" className={`d-flex align-items-center mb-2`}>
        <DateRangeIcon /> {carPost?.createdAt && carPost.createdAt.split('.')[0].split('T')[0]}{' '}
        <AccessTimeIcon />
        {carPost?.createdAt && carPost.createdAt.split('.')[0].split('T')[1]}
      </Typography>
      <Typography variant="body2" className={`d-flex align-items-center mb-2`}>
        <AddRoadIcon /> Lat: {carPost?.latitude} <MultipleStopIcon /> Long: {carPost?.longitude}
      </Typography>
    </CardContent>
  ) : (
    <CardContent>
      <Typography gutterBottom component="div" variant="body1">
        <LoadingSpinner />
      </Typography>
    </CardContent>
  );
};

export default CarInformation;
