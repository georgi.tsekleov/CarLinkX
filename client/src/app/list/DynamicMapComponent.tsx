'use client';
import Skeleton from '@mui/material/Skeleton';
import dynamic from 'next/dynamic';
import { Post } from '../types';

const Dynamic = dynamic(() => import('./MapComponent'), {
  ssr: false,
  loading: () => <Skeleton variant="rectangular" className={`w-100 h-100`} />
});

const DynamicMapComponent = ({ carsList }: { carsList: Post[] }) => {
  return <Dynamic carsList={carsList} />;
};
export default DynamicMapComponent;
