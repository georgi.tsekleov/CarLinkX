import { StaticImageData } from 'next/image';

export interface Car {
  id: string;
  title: string;
  img: StaticImageData;
  price: string;
  location: {
    lat: number;
    lng: number;
  };
  owner: string;
}

export interface SelectionState {
  price: number[];
  region: string;
}
