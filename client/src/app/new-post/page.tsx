'use client';

import React, { ChangeEvent, FormEvent, useState } from 'react';
import {
  TextField,
  Button,
  MenuItem,
  Container,
  Grid,
  Badge,
  CircularProgress
} from '@mui/material';
import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import { CreateNewPost as NewPost } from '@/app/types';
import UploadWidget from '@/components/uploadWidget/UploadWidget';
import { UploadedWidgetImg } from '@/types';
import { CLOUD_NAME, MAX_SIZE_ONE_MB, UPLOAD_PRESET } from '../../../state';
import { useUserAuth } from '@/context/AuthContex';
import CropOriginalIcon from '@mui/icons-material/CropOriginal';

const initialFormDataState: NewPost = {
  title: '',
  price: '',
  address: '',
  city: '',
  description: '',
  vehicle: 'car',
  model: '',
  latitude: '',
  longitude: '',
  images: []
};

type ResponseHandler = 'idle' | 'success' | 'fail';
// TODO: Refactor code by components with composition!!!

const CreateNewPost = () => {
  const { currentUser } = useUserAuth();
  const [formData, setFormData] = useState<NewPost>(initialFormDataState);
  const [uploadedImages, setUploadedImages] = useState<UploadedWidgetImg[]>([]);
  const isUploadedImages = uploadedImages.length > 0;
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState<ResponseHandler>('idle');

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setFormData({
      ...formData,
      images: value.split(',')
    });
  };

  const handleImageUpload = (imgArray: UploadedWidgetImg[]) => {
    setUploadedImages((prevUploadedImages) => [...prevUploadedImages, ...imgArray]);
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);

    const convertedDormData = {
      ...formData,
      price: parseInt(formData.price, 10),
      latitude: formData.latitude.toString(),
      longitude: formData.longitude.toString(),
      images: uploadedImages
    };

    try {
      const response = await fetch(`api/new-post`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({ body: convertedDormData, tokenUserId: currentUser?.id }),
        credentials: 'include'
      });

      if (response.ok) {
        setFormData(initialFormDataState);
        setUploadedImages([]);
        setResponse('success');
      } else {
        console.log('Fail to upload data!s');
        setResponse('fail');
      }
    } catch (err) {
      console.log('Error on upload new post data: ', err);
      setResponse('fail');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SectionMUI className={`px-4`}>
      <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
        <Container>
          <h1>Create New Post</h1>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={10}>
                <TextField
                  label="Title"
                  name="title"
                  value={formData.title}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={2}>
                <TextField
                  label="Price"
                  name="price"
                  type="number"
                  value={formData.price}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Address"
                  name="address"
                  value={formData.address}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="City"
                  name="city"
                  value={formData.city}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Latitude"
                  name="latitude"
                  type="number"
                  value={formData.latitude}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Longitude"
                  name="longitude"
                  type="number"
                  value={formData.longitude}
                  onChange={handleChange}
                  fullWidth
                  required
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  select
                  label="Vehicle"
                  name="vehicle"
                  value={formData.vehicle}
                  onChange={handleChange}
                  fullWidth
                  required
                >
                  {['motorcycle', 'car', 'van', 'bus'].map((option) => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  select
                  label="Model"
                  name="model"
                  value={formData.model}
                  onChange={handleChange}
                  fullWidth
                  required
                >
                  {['Mercedes', 'Audi', 'BMW', 'Toyota'].map((option) => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="Description"
                  name="description"
                  value={formData.description}
                  onChange={handleChange}
                  fullWidth
                  required
                  multiline
                  rows={4}
                />
              </Grid>

              <Grid item xs={12} className={`custom-flex-center`}>
                {isUploadedImages ? (
                  <>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={`w-50 fs-5`}
                      disabled={loading}
                    >
                      {loading ? (
                        <CircularProgress color="primary" className={`ms-2`} />
                      ) : (
                        <>
                          Create New Post
                          <Badge
                            badgeContent={uploadedImages.length}
                            color="secondary"
                            className={`ms-2`}
                          >
                            <CropOriginalIcon />
                          </Badge>
                        </>
                      )}
                    </Button>
                  </>
                ) : (
                  <UploadWidget
                    uwConfig={{
                      cloudName: CLOUD_NAME,
                      uploadPreset: UPLOAD_PRESET,
                      maxImageFileSize: MAX_SIZE_ONE_MB,
                      multiple: true,
                      folder: `posts-${currentUser?.email}`
                    }}
                    setState={handleImageUpload}
                    showUploadBtn={true}
                  />
                )}
              </Grid>

              <Grid item xs={12}>
                <p className={`text-center text-success`}>
                  {response === 'success' && 'Successfully upload!'}
                </p>
                <p className={`text-center text-danger`}>
                  {response === 'fail' && 'Upload failed!'}
                </p>
              </Grid>
            </Grid>
          </form>
        </Container>
      </MaxWidthWrapper>
    </SectionMUI>
  );
};

export default CreateNewPost;
