import styles from './page.module.scss';
import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import LandingInformation from '@/components/home/LandingInformation';

const Home = () => {
  return (
    <main className={`${styles.Main}`}>
      <SectionMUI className={`px-2`}>
        <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
          <h1 className={`text-center mb-4 mb-md-5`}>
            Discover <span className={`gradient-primary-text`}>Future of Connectivity</span>
          </h1>
          <LandingInformation />
        </MaxWidthWrapper>
      </SectionMUI>
    </main>
  );
};

export default Home;
