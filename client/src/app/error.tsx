'use client';

import React, { useEffect } from 'react';

interface ErrorProps {
  error: Error & {
    digest?: string;
  };
  reset: () => void;
}

const Error = ({ error, reset }: ErrorProps) => {
  useEffect(() => {
    console.error(error);
  }, [error]);

  return (
    <section className="text-center bg-success text-white rounded m-5">
      <h2 className="text-white">Something wrong!</h2>
      <button className="btn btn-primary mb-2" onClick={() => reset()}>
        Try again
      </button>
    </section>
  );
};

export default Error;
