import { StaticImageData } from 'next/image';

export type VehicleType = 'motorcycle' | 'car' | 'van' | 'bus';

export interface CreateNewPost {
  id?: string;
  title: string;
  price: string;
  address: string;
  city: string;
  description: string;
  vehicle: VehicleType;
  model: string;
  latitude: string;
  longitude: string;
  images: string[];
  createdAt?: string;
  userId?: string;
}

export interface Post extends CreateNewPost {
  user: {
    avatar: StaticImageData[];
    username: string;
    email: string;
  };
}
