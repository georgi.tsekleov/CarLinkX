import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';

const Page = () => {
  return (
    <SectionMUI className={`px-4`}>
      <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
        <h1>Page - contacts</h1>
      </MaxWidthWrapper>
    </SectionMUI>
  );
};

export default Page;
