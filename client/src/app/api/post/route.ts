import { NextResponse } from 'next/server';
import prisma from '../../../../prisma/client';

//BE-API-4
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { id } = await req.json();

    const post = await prisma.post.findUnique({
      where: { id },
      include: {
        user: {
          select: {
            username: true,
            avatar: true,
            email: true
          }
        }
      }
    });

    return NextResponse.json(post, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to get post' }, { status: 500 });
  }
};
