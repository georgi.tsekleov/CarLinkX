import { NextRequest, NextResponse } from 'next/server';
import bcrypt from 'bcrypt';
import prisma from '../../../../../prisma/client';

// TODO: Add this route and show register button if have free vercel BE functions
//BE-API-?
export const POST = async (req: NextRequest) => {
  const { username, email, password } = await req.json();

  try {
    const isUserExist = await prisma.user.findUnique({
      where: { username }
    });
    if (isUserExist)
      return NextResponse.json({ message: `User: ${username} already exists!` }, { status: 401 });

    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = await prisma.user.create({
      data: {
        username,
        email,
        password: hashedPassword
      }
    });
    console.log('New user created successfully:', newUser);

    return NextResponse.json({ message: 'User created successfully!' }, { status: 201 });
  } catch (err) {
    console.error('Failed to create user:', err);
    return NextResponse.json({ message: 'Failed to create user!' }, { status: 500 });
  }
};
