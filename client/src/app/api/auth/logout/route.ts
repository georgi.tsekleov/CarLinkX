import { NextResponse } from 'next/server';

//BE-API-2
export const POST = () => {
  const response = NextResponse.json({ message: 'Logout Successful' }, { status: 200 });
  response.cookies.set('token', '', { httpOnly: true, maxAge: 0 });

  return response;
};
