import { NextRequest, NextResponse } from 'next/server';
import bcrypt from 'bcrypt';
import prisma from '../../../../../prisma/client';
import jwt from 'jsonwebtoken';

//BE-API-1
export const POST = async (req: NextRequest) => {
  const { username, password } = await req.json();

  try {
    const dbUser = await prisma.user.findUnique({
      where: { username }
    });
    if (!dbUser) {
      return NextResponse.json({ message: 'Invalid Credentials!' }, { status: 401 });
    }

    const isValidPassword = await bcrypt.compare(password, dbUser.password);
    if (!isValidPassword) {
      return NextResponse.json({ message: 'Invalid Credentials!' }, { status: 401 });
    }

    const age = 1000 * 60 * 60 * 24 * 7;
    const token = jwt.sign(
      { id: dbUser.id, isAdmin: false },
      process.env.JWT_SECRET_KEY as string,
      {
        expiresIn: age
      }
    );

    const { password: dbUserPassword, ...restUserInfo } = dbUser;

    const response = NextResponse.json(
      {
        message: 'Login Successful',
        user: restUserInfo
      },
      { status: 200 }
    );

    response.cookies.set('token', token, { httpOnly: true, maxAge: age });
    return response;
  } catch (err) {
    console.error('Failed to login:', err);
    return NextResponse.json({ message: 'Failed to login!' }, { status: 500 });
  }
};
