import { NextResponse } from 'next/server';
import prisma from '../../../../../prisma/client';

//BE-API-7
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { chatId, tokenUserId } = await req.json();

    const singleChat = await prisma.chat.findUnique({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [tokenUserId]
        }
      },
      include: {
        messages: {
          orderBy: {
            createdAt: 'asc'
          }
        }
      }
    });

    return NextResponse.json(singleChat, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to get post' }, { status: 500 });
  }
};
