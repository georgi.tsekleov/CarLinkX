import { NextResponse } from 'next/server';
import prisma from '../../../../../prisma/client';

//BE-API-7
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { chatId, currentUserId } = await req.json();

    console.log('read-chat', chatId, currentUserId);
    const readChat = await prisma.chat.update({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [currentUserId]
        }
      },
      data: {
        seenBy: {
          set: [currentUserId]
        }
      }
    });
    
    console.log('read-chat - readChat', readChat);

    return NextResponse.json(readChat, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to get post' }, { status: 500 });
  }
};

/*
export const readChat = async (req, res) => {
  const tokenUserId = req.userId;
  const chatId = req.params.id;

  try {
    const readChat = await prisma.chat.update({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [tokenUserId]
        }
      },
      data: {
        seenBy: {
          set: [tokenUserId]
        }
      }
    });

    res.status(200).json(readChat);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get read chat!' });
  }
};
 */
