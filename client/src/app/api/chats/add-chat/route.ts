import { NextResponse } from 'next/server';
import prisma from '../../../../../prisma/client';

//BE-API-10
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { receiverId, currentUserId } = await req.json();

    const newChat = await prisma.chat.create({
      data: { userIDs: [currentUserId, receiverId] }
    });
    console.log('newChat ', newChat);

    return NextResponse.json(newChat, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to add chat' }, { status: 500 });
  }
};
