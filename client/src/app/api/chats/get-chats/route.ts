import { NextResponse } from 'next/server';
import prisma from '../../../../../prisma/client';

//BE-API-5
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { userId } = await req.json();

    const chats = await prisma.chat.findMany({
      where: {
        userIDs: { hasSome: [userId] }
      }
    });

    const updatedChats = await Promise.all(
      chats.map(async (chat) => {
        const receiverId = chat.userIDs.find((id) => id !== userId);

        const receiver = await prisma.user.findUnique({
          where: { id: receiverId },
          select: {
            id: true,
            username: true,
            avatar: true
          }
        });

        return {
          ...chat,
          receiver
        };
      })
    );

    return NextResponse.json(updatedChats, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to get post' }, { status: 500 });
  }
};
