import { NextResponse } from 'next/server';
import prisma from '../../../../prisma/client';

export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { body, tokenUserId } = await req.json();

    const newPost = await prisma.post.create({
      data: {
        ...body,
        userId: tokenUserId
      }
    });

    return NextResponse.json(newPost, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to create post' }, { status: 500 });
  }
};
