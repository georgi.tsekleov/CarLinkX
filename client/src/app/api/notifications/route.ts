import { NextResponse } from 'next/server';
import prisma from '../../../../prisma/client';

//BE-API-4
export const POST = async (req: Request, res: NextResponse) => {
  try {
    const { tokenUserId } = await req.json();

    const notificationNumber = await prisma.chat.count({
      where: {
        userIDs: {
          hasSome: [tokenUserId]
        },
        NOT: {
          seenBy: {
            hasSome: [tokenUserId]
          }
        }
      }
    });

    return NextResponse.json(notificationNumber, { status: 200 });
  } catch (err) {
    console.error('Failed to get post:', err);
    return NextResponse.json({ message: 'Failed to get post' }, { status: 500 });
  }
};
