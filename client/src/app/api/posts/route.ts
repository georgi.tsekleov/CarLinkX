import prisma from '../../../../prisma/client';
import { NextResponse } from 'next/server';

//BE-API-3
export const GET = async () => {
  try {
    const posts = await prisma.post.findMany({
      include: {
        user: {
          select: {
            username: true,
            avatar: true,
            email: true
          }
        }
      }
    });

    if (!posts) {
      return NextResponse.json({ message: 'No fetched posts!' }, { status: 401 });
    }

    return NextResponse.json(posts, { status: 200 });
  } catch (err) {
    console.error('Failed to get posts:', err);
    return NextResponse.json({ message: 'Failed to get posts!' }, { status: 500 });
  }
};
