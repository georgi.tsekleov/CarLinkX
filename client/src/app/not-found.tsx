import React from "react";
import Link from "next/link";
import styles from "./page.module.scss";

const NotFound = () => {
  return (
    <div
      className={`vh-100  d-flex flex-column text-center justify-content-center align-items-center ${styles.NotFound}`}
    >
      <h2 className="m-0 text-primary"> 404</h2>
      <h3 className="m-0"> Error! Page is not found!</h3>
      <p>
        <Link className={`text-primary`} href={`/`}>
          Back to home page
        </Link>
      </p>
    </div>
  );
};

export default NotFound;
