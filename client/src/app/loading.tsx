import LoadingSpinner from '@/components/loading/LoadingSpinner';

export default function Loading() {
  return (
    <div>
      <LoadingSpinner />
    </div>
  );
}
