import React, { useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { Box, Typography } from '@mui/material';
import useFetchPosts from '@/components/common/hooks/useFetchPosts';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { useUserAuth } from '@/context/AuthContex';
import SinglePost from './SinglePost';

type FilterPosts = 'my' | 'others' | 'all';

const SamplePosts = () => {
  const { posts, loading, error } = useFetchPosts();
  const [filter, setFilter] = useState<FilterPosts>('all');
  const { currentUser } = useUserAuth();

  if (loading) {
    return <CircularProgress />;
  }

  if (error || !posts) {
    return <Typography variant="body1">Error fetching posts. Please try again later.</Typography>;
  }

  const handleFilter = (type: FilterPosts) => {
    setFilter(type);
  };

  const filteredPosts = posts.filter((post) => {
    if (filter === 'my') {
      return post.userId === currentUser?.id;
    } else if (filter === 'others') {
      return post.userId !== currentUser?.id;
    } else {
      return true;
    }
  });

  return (
    <Box>
      <Box className={`custom-flex-center mb-3`}>
        <ButtonGroup variant="text" aria-label="Basic button group" className={`bg-white`}>
          <Button
            onClick={() => handleFilter('my')}
            className={`${filter === 'my' ? 'text-dark' : ''}`}
          >
            My
          </Button>
          <Button
            onClick={() => handleFilter('others')}
            className={`${filter === 'others' ? 'text-dark' : ''}`}
          >
            Others
          </Button>
          <Button
            onClick={() => handleFilter('all')}
            className={`${filter === 'all' ? 'text-dark' : ''}`}
          >
            All
          </Button>
        </ButtonGroup>
      </Box>

      {filteredPosts.length === 0 ? (
        <Typography variant="body1" className={`m-3 text-white text-center`}>
          No posts found.
        </Typography>
      ) : (
        filteredPosts.map((post, index) => <SinglePost key={index} post={post} />)
      )}
    </Box>
  );
};
export default SamplePosts;
