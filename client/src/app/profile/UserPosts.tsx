'use client';
import React from 'react';
import styles from './UserPosts.module.scss';
import SamplePosts from '@/app/profile/SamplePosts';
import { Typography } from '@mui/material';
import Container from '@mui/material/Container';

const UserPosts = () => {
  return (
    <Container className={`mt-3 px-3 ${styles.MyPosts}  bg-primary rounded-4 pb-3`}>
      <Typography variant="h2" className={`text-center text-white`}>
        Post List
      </Typography>

      <SamplePosts />
    </Container>
  );
};

export default UserPosts;
