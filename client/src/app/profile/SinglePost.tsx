import { Post } from '@/app/types';
import { Box, Paper, Typography } from '@mui/material';
import Image from 'next/image';
import ErrorImage from '../../../images/error/problem_preview.png';
import { format } from 'timeago.js';
import DynamicLinkBtn from '@/app/list/DynamicLinkBtn';
import React from 'react';
import DefImage from '../../../images/profile/def-avatar.png';

const SinglePost = ({ post }: { post: Post }) => {
  return (
    <Paper className={`position-relative post-item border rounded-2 py-3 px-3 mt-3 bg-white`}>
      <Typography variant="h3" className={`mt-3`}>
        {post.title}
      </Typography>
      <Typography className={`m-0`}>
        Price: <b>${post.price}</b>
      </Typography>
      <Typography className={`m-0`}>
        Address: <b>{post.address}</b>
      </Typography>
      <Typography className={`m-0`}>
        City:<b> {post.city}</b>
      </Typography>
      <Typography className={`m-0`}>
        Vehicle: <b>{post.vehicle}</b>
      </Typography>
      <Typography className={`m-0`}>
        Model:<b> {post.model}</b>
      </Typography>
      <Typography className={`m-0 mt-2`}>Description: {post.description}</Typography>
      <Box>
        {post.images.map((image, imageIndex) => (
          <Image
            width={150}
            height={100}
            key={imageIndex}
            src={image ? image : ErrorImage}
            alt={`Image ${imageIndex + 1}`}
            className={`m-2 rounded-3`}
          />
        ))}
      </Box>

      {post.createdAt && (
        <>
          <Typography className={`ms-2 mt-1 position-absolute top-0 start-0`}>
            <Image
              src={post.user.avatar[0] || DefImage}
              alt={post.model}
              width={25}
              height={25}
              className={`me-1 rounded-5`}
            />
            {post.user.username}
          </Typography>

          <Typography className={`me-2 mt-1 position-absolute top-0 end-0`}>
            {format(post.createdAt)}
          </Typography>
        </>
      )}

      <Box className={`mt-4 mt-md-0`}></Box>
      <DynamicLinkBtn carId={post.id} />
    </Paper>
  );
};
export default SinglePost;
