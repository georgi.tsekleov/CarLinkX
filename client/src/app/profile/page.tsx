'use client';

import React from 'react';
import { useUserAuth } from '@/context/AuthContex';
import UserAvatar from '@/components/common/avatar/UserAvatar';
import UserPosts from '@/app/profile/UserPosts';
import SectionMUI from '@/components/SectionMUI';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import CustomContainer from '@/components/common/gridContainer/CustomContainer';
import CustomCol from '@/components/common/gridContainer/CustomCol';
import LogOutBtn from '@/components/header/register/LogOutBtn';
import CustomRow from '@/components/common/gridContainer/CustomRow';
import LoadingSpinner from '@/components/loading/LoadingSpinner';
import { Box, Typography } from '@mui/material';

const Page = () => {
  const { currentUser } = useUserAuth();

  return (
    <SectionMUI className={`px-4`}>
      <MaxWidthWrapper sx={{ flexDirection: 'column' }}>
        {currentUser ? (
          <CustomContainer>
            <CustomRow>
              <CustomCol xs={12} className={`p-0 custom-flex-center flex-column`}>
                <Box>
                  <LogOutBtn />
                </Box>

                <UserAvatar currentUser={currentUser} showUploadBtn={true} width={200} height={200}>
                  <Box className={`flex-column`}>
                    <Typography variant="body1" className={`m-0`}>
                      Username: {currentUser?.username}
                    </Typography>
                    <Typography variant="body1" className={`m-0`}>
                      ID: {currentUser?.id}
                    </Typography>
                    <Typography variant="body1" className={`m-0`}>
                      Email: {currentUser?.email}
                    </Typography>
                  </Box>
                </UserAvatar>
              </CustomCol>

              <CustomCol xs={12} className={`p-0 mt-3 mt-md-0`}>
                <UserPosts />
              </CustomCol>
            </CustomRow>
          </CustomContainer>
        ) : (
          <LoadingSpinner />
        )}
      </MaxWidthWrapper>
    </SectionMUI>
  );
};
export default Page;
