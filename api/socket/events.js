// class SocketEvents {
//   constructor(io, userManagement) {
//     this.io = io;
//     this.userManagement = userManagement;
//   }
//
//   handleConnection(socket) {
//     console.log("Socket client connected with id: ", socket.id);
//
//     socket.on("sendMessage", ({ receiverId, data }) => {
//       this.handleSendMessage(receiverId, data);
//     });
//
//     socket.on("newUser", (userId) => {
//       this.userManagement.addUser(userId, socket.id);
//     });
//
//     socket.on("disconnect", () => {
//       console.log("Client disconnected:", socket.id);
//       this.userManagement.removeUser(socket.id);
//     });
//   }
//
//   handleSendMessage(receiverId, data) {
//     const receiver = this.userManagement.getUser(receiverId);
//     if (receiver) {
//       this.io.to(receiver.socketId).emit("getMessage", data);
//     }
//   }
// }
//
// export default SocketEvents;
