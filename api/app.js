import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import http from 'http';
import { Server } from 'socket.io';
import postRoute from './routes/post.route.js';
import authRoute from './routes/auth.route.js';
import testRoute from './routes/test.route.js';
import usersRoute from './routes/user.route.js';
import chatRoute from './routes/chat.route.js';
import messageRoute from './routes/message.route.js';
import UserManagement from './socket/userManagement.js';
import SocketEvents from './socket/events.js';

const app = express();
const server = http.createServer(app);

app.use(cors({ origin: process.env.CLIENT_URL, credentials: true }));
app.use(express.json());
app.use(cookieParser());

app.use('/api/posts', postRoute);
app.use('/api/auth', authRoute);
app.use('/api/test', testRoute);
app.use('/api/users', usersRoute);
app.use('/api/chats', chatRoute);
app.use('/api/messages', messageRoute);

const io = new Server(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST']
  }
});

const userManagement = new UserManagement();
const socketEvents = new SocketEvents(io, userManagement);

io.on('connection', (socket) => socketEvents.handleConnection(socket));

server.listen(8800, () => {
  console.log('Server is running');
});

io.listen(4000, () => {
  console.log('Socket.io server is listening on port 4000');
});
