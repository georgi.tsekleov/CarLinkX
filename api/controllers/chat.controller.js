import prisma from '../lib/prisma.js';

export const getChats = async (req, res) => {
  const tokenUserId = req.userId;

  try {
    const chats = await prisma.chat.findMany({
      where: {
        userIDs: { hasSome: [tokenUserId] }
      }
    });

    for (const chat of chats) {
      const receiverId = chat.userIDs.find((id) => id !== tokenUserId);

      chat.receiver = await prisma.user.findUnique({
        where: { id: receiverId },
        select: {
          id: true,
          username: true,
          avatar: true
        }
      });
    }

    res.status(200).json(chats);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get chats!' });
  }
};

export const getChat = async (req, res) => {
  const tokenUserId = req.userId;
  const chatId = req.params.id;

  try {
    const singleChat = await prisma.chat.findUnique({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [tokenUserId]
        }
      },
      include: {
        messages: {
          orderBy: {
            createdAt: 'asc'
          }
        }
      }
    });

    await prisma.chat.update({
      where: { id: chatId },
      data: {
        seenBy: {
          set: [tokenUserId]
        }
      }
    });
    res.status(200).json(singleChat);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get chat!' });
  }
};

export const AddChat = async (req, res) => {
  const tokenUserId = req.userId;

  try {
    const newChat = await prisma.chat.create({
      data: { userIDs: [tokenUserId, req.body.receiverId] }
    });
    console.log('newChat ', newChat);
    res.status(200).json(newChat);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get add chat!' });
  }
};

export const readChat = async (req, res) => {
  const tokenUserId = req.userId;
  const chatId = req.params.id;

  try {
    const readChat = await prisma.chat.update({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [tokenUserId]
        }
      },
      data: {
        seenBy: {
          set: [tokenUserId]
        }
      }
    });

    res.status(200).json(readChat);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get read chat!' });
  }
};
