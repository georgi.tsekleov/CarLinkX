import bcrypt from 'bcrypt';
import prisma from '../lib/prisma.js';
import jtw from 'jsonwebtoken';

export const register = async (req, res) => {
  const { username, email, password } = req.body;

  try {
    const isUserExist = await prisma.user.findUnique({
      where: { username }
    });
    if (isUserExist) return res.status(401).json({ message: `User: ${username} already exist!` });

    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = await prisma.user.create({
      data: {
        username,
        email,
        password: hashedPassword
      }
    });
    console.log('newUser is created successfully! ', newUser);

    res.status(201).json({ message: 'User is created successfully!' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: 'Fail to create user!' });
  }
};

export const login = async (req, res) => {
  const { username, password } = req.body;

  try {
    const dbUser = await prisma.user.findUnique({
      where: { username }
    });
    if (!dbUser) return res.status(401).json({ message: 'Invalid Credentials!' });

    const isValidPassword = await bcrypt.compare(password, dbUser.password);
    if (!isValidPassword) return res.status(401).json({ message: 'Invalid Credentials!' });

    const age = 1000 * 60 * 60 * 24 * 7;
    const token = jtw.sign({ id: dbUser.id, isAdmin: false }, process.env.JWT_SECRET_KEY, {
      expiresIn: age
    });

    const { password: dbUserPassword, ...restUserInfo } = dbUser;

    res.cookie('token', token, { httpOnly: true, maxAge: age }).status(200).json({
      message: 'Login Successful',
      user: restUserInfo
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: 'Failed to login!' });
  }
};

export const logout = (req, res) => {
  res.clearCookie('token').status(200).json({ message: 'Logout Successful' });
};
