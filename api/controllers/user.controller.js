import prisma from '../lib/prisma.js';
import bcrypt from 'bcrypt';

export const getUsers = async (req, res) => {
  try {
    const users = await prisma.user.findMany();
    res.status(200).json(users);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Failed to get socket!' });
  }
};

export const getUser = async (req, res) => {
  const id = req.params.id;
  try {
    const user = await prisma.user.findUnique({
      where: { id }
    });
    console.log('user-found: ', user);

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({ message: 'User not found!' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Failed to get user!' });
  }
};

export const updateUser = async (req, res) => {
  const id = req.params.id;
  const tokenLoggedInUserId = req.userId;
  const { password, avatar, ...bodyInputs } = req.body;

  if (id !== tokenLoggedInUserId) {
    res.status(403).json({ message: 'Not authorized to update user!' });
  }

  let updatedHashPassword = null;
  try {
    if (password) {
      updatedHashPassword = await bcrypt.hash(password, 10);
    }

    const updatedUser = await prisma.user.update({
      where: { id },
      data: {
        ...bodyInputs,
        ...(updatedHashPassword && { password: updatedHashPassword }),
        ...(avatar && { avatar })
      }
    });

    res.status(200).json(updatedUser);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Failed to update user!' });
  }
};

export const deleteUser = async (req, res) => {
  const id = req.params.id;
  const tokenLoggedInUserId = req.userId;

  if (id !== tokenLoggedInUserId) {
    res.status(403).json({ message: 'Not authorized to delete user!' });
  }

  try {
    await prisma.user.delete({ where: { id } });
    res.status(200).json({ message: 'User deleted!' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Failed to delete user!' });
  }
};

export const getNotification = async (req, res) => {
  const tokenUserId = req.userId;

  try {
    const notificationNumber = await prisma.chat.count({
      where: {
        userIDs: {
          hasSome: [tokenUserId]
        },
        NOT: {
          seenBy: {
            hasSome: [tokenUserId]
          }
        }
      }
    });
    res.status(200).json(notificationNumber);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Failed to get notifications!' });
  }
};
