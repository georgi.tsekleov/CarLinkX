import prisma from '../lib/prisma.js';

export const AddMessage = async (req, res) => {
  const tokenUserId = req.userId;
  const chatId = req.params.chatId;
  const text = req.body.text;

  try {
    const chat = await prisma.chat.findUnique({
      where: {
        id: chatId,
        userIDs: {
          hasSome: [tokenUserId]
        }
      }
    });

    if (!chat) res.status(404).json({ message: 'Chat not found!' });

    const message = await prisma.message.create({
      data: {
        text,
        chatId,
        userId: tokenUserId
      }
    });

    console.log('message ', message);
    console.log('chat ', chat);

    const updatedChat = await prisma.chat.update({
      where: { id: chatId },
      data: {
        seenBy: [tokenUserId],
        lastMessage: text
      }
    });
    console.log('updatedChat ', updatedChat);

    res.status(200).json(message);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get add message!' });
  }
};
