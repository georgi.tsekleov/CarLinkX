import prisma from '../lib/prisma.js';

export const getPosts = async (req, res) => {
  try {
    const posts = await prisma.post.findMany();

    res.status(200).json(posts);
  } catch (err) {
    console.log('Fail to get posts: ', err);
    res.status(500).json({ message: 'Fail to get posts!' });
  }
};
export const getPost = async (req, res) => {
  const id = req.params.id;
  try {
    const post = await prisma.post.findUnique({
      where: { id },
      include: {
        user: {
          select: {
            username: true,
            avatar: true,
            email: true
          }
        }
      }
    });

    res.status(200).json(post);
  } catch (err) {
    console.log('Fail to get post: ', err);
    res.status(500).json({ message: 'Fail to get post!' });
  }
};

export const AddPost = async (req, res) => {
  const body = req.body;
  const tokenUserId = req.userId;

  try {
    const newPost = await prisma.post.create({
      data: {
        ...body,
        userId: tokenUserId
      }
    });

    console.log('newPost ', newPost);

    res.status(200).json(newPost);
  } catch (err) {
    console.log('Fail to add post: ', err);
    res.status(500).json({ message: 'Fail to add posts!' });
  }
};

export const updatePost = async (req, res) => {
  console.log('updatePost');

  try {
    const post = await prisma.post.findMany();

    res.status(200).json(post);
  } catch (err) {
    console.log('Fail to update post: ', err);
    res.status(500).json({ message: 'Fail to update post!' });
  }
};

export const deletePost = async (req, res) => {
  const id = req.params.id;
  const tokenUserId = req.userId;

  try {
    const post = await prisma.post.findUnique({ where: { id } });

    if (post.userId !== tokenUserId) {
      return res.status(403).json({ message: 'Not authorized to delete post' });
    }

    await prisma.post.delete({ where: { id } });

    res.status(200).json({ message: 'Post deleted' });
  } catch (err) {
    console.log('Fail to delete post: ', err);
    res.status(500).json({ message: 'Fail to delete post!' });
  }
};
